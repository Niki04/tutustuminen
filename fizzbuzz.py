for num in range(1,101):
  #Tarkistaa että luku on jaollinen sekä 3 ja 5:llä
  if(num%3==0 and num%5==0):
    print("FizzBuzz")
  #Tarkistaa että luku on jaollinen kolmella
  elif(num%3 == 0):
    print("Fizz")
  #Tarkistaa että luku on jaollinen viidellä
  elif(num%5 == 0):
    print("Buzz")
  else:
    print(num)